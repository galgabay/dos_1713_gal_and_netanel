    global loader                   ; the entry symbol for ELF  

    MAGIC_NUMBER equ 0x1BADB002     ; define the magic number constant
    ALIGN_MODULES   equ 0x00000001
    FLAGS        equ 0x0            ; multiboot flags
    CHECKSUM        equ -(MAGIC_NUMBER + ALIGN_MODULES)
    KERNEL_STACK_SIZE equ 4096 			  ; calculate the 							
                                    ; (magic number + checksum + flags should equal 0)

    section .text:                  ; start of the text (code) section
    extern kmain
    align 4                         ; the code must be 4 byte aligned
        dd MAGIC_NUMBER             ; write the magic number to the machine code,
        dd FLAGS                    ; the flags,
	dd ALIGN_MODULES
        dd CHECKSUM
	mov esp, kernel_stack + KERNEL_STACK_SIZE                  

    loader:                         ; the loader label (defined as 					entry point in linker script)
        mov eax, 0xCAFEBABE         ; place the number 0xCAFEBABE in 		the register eax
   	call kmain
	cli
	lgdt, [eax]
	mov ds, 0x10
        mov ss, 0x10
        mov es, 0x10
	jmp 0x08:flush_cs
    .loop:
        jmp .loop	                   ; loop forever
     
    flush_cs:


    section .bss
        align 4                                     ; align at 4 bytes
        kernel_stack:                               ; label points to 	      beginning of memory
        resb KERNEL_STACK_SIZE 




%macro no_error_code_interrupt_handler %1
    global interrupt_handler_%1
    interrupt_handler_%1:
        push    dword 0                     ; push 0 as error code
        push    dword %1                    ; push the interrupt number
        jmp     common_interrupt_handler    ; jump to the common handler
    %endmacro

    %macro error_code_interrupt_handler %1
    global interrupt_handler_%1
    interrupt_handler_%1:
        push    dword %1                    ; push the interrupt number
        jmp     common_interrupt_handler    ; jump to the common handler
    %endmacro

    common_interrupt_handler:               ; the common parts of the generic interrupt handler
        ; save the registers
        push    eax
        push    ebx

        push    ebp

        ; call the C function
        call    interrupt_handler

        ; restore the registers
        pop     ebp

        pop     ebx
        pop     eax

        ; restore the esp
        add     esp, 8

        ; return to the code that got interrupted
        iret

    no_error_code_interrupt_handler 0       ; create handler for interrupt 0
    no_error_code_interrupt_handler 1       ; create handler for interrupt 1
    error_code_handler              7       ; create handler for interrupt 7


     global  load_idt

    ; load_idt - Loads the interrupt descriptor table (IDT).
    ; stack: [esp + 4] the address of the first entry in the IDT
    ;        [esp    ] the return address
    load_idt:
        mov     eax, [esp+4]    ; load the address of the IDT into register eax
        lidt    eax             ; load the IDT
        ret                     ; return to the calling function 
